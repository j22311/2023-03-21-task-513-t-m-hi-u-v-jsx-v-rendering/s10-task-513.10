// import logo from './logo.svg';
// import './App.css';

import { gDevcampReact, tylesinhvien } from "./info";

function App() {
  return (
    <div className="App" >
      <h1>
        {gDevcampReact.title}
      </h1>
      <img src={gDevcampReact.image} width="500px"></img>
      <p>tỷ lệ sinh viên theo học: {tylesinhvien()} %</p>
      {tylesinhvien() > 15 ? "sinh viên đăng ký học nhiều" : "sinh viên đăng ký học ít"}

      <ul>
        {
          gDevcampReact.benefits.map((value,index) => {
            return <li>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
